# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

stages:
  - compliance
  - test
  - build
  - integration
  - deploy

include:
  - template: 'Workflows/MergeRequest-Pipelines.gitlab-ci.yml'
  - project: 'zygoon/go-gitlab-ci'
    ref: v0.4.0
    file: '/go.yml'

dco:
  interruptible: true
  stage: compliance
  image: christophebedard/dco-check:latest
  tags: [linux, docker, x86_64]
  rules:
    - if: $CI_MERGE_REQUEST_ID
    - if: $CI_EXTERNAL_PULL_REQUEST_IID
    - if: $CI_COMMIT_BRANCH == '$CI_DEFAULT_BRANCH'
  script:
    # Work around a bug in dco-check affecting pipelines for merge requests.
    # https://github.com/christophebedard/dco-check/issues/104
    - |
        if [ "${CI_MERGE_REQUEST_EVENT_TYPE:-}" = detached ]; then
            git fetch -a  # so that we can resolve branch names below
            export CI_COMMIT_BRANCH="$CI_COMMIT_REF_NAME";
            export CI_COMMIT_BEFORE_SHA="$CI_MERGE_REQUEST_DIFF_BASE_SHA";
            export CI_MERGE_REQUEST_SOURCE_BRANCH_SHA="$(git rev-parse "origin/$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME")";
            export CI_MERGE_REQUEST_TARGET_BRANCH_SHA="$(git rev-parse "origin/$CI_MERGE_REQUEST_TARGET_BRANCH_NAME")";
        fi
    - dco-check --default-branch-from-remote --verbose

reuse:
  interruptible: true
  tags: [linux, docker, x86_64]
  stage: compliance
  image:
    name: fsfe/reuse:latest
    entrypoint: [""]
  script:
    - reuse lint

.go:
  variables:
    # Use latest Go for building binaries that we package and ship. This is
    # coupled with some minimal risk of regressions, when future Go finds
    # something that is broken about current code, but given the stability of
    # the language, this provides the best outcome for the users.
    CI_GO_VERSION: "latest"

.go-build:
  variables:
    # This is already done by go-gitlab-ci but setting it explicitly makes it
    # easier to "git grep" for the value and see where it is used. In some
    # places using variables is hard but it is still important to have a
    # synchronized behavior.
    GOFLAGS: -ldflags=-s -ldflags=-w -trimpath
    # Disable cgo to avoid linking to host libc.
    CGO_ENABLED: "0"

go-test:
  parallel:
    # Test against both 1.18 and latest go.
    matrix:
      - CI_GO_VERSION: ["1.18", latest]

# Build specific binaries so that we can use them in integration testing and
# docker containers later. Those cannot be build with the parallel/matrix jobs
# because we need to be able to depend on them, This is a gitlab-runner
# limitation.
build/linux/x86_64:
  extends: .go-build
  variables:
    GOOS: linux
    GOARCH: amd64
  before_script:
    - !reference [.go-build, before_script]
    - echo "LINUX_X86_64_CI_JOB_ID=$CI_JOB_ID" >artifacts.linux.x86_64.env
  artifacts:
    paths: [bin]
    reports:
      dotenv: artifacts.linux.x86_64.env

build/linux/aarch64:
  extends: .go-build
  variables:
    GOOS: linux
    GOARCH: arm64
  before_script:
    - !reference [.go-build, before_script]
    - echo "LINUX_AARCH64_CI_JOB_ID=$CI_JOB_ID" >artifacts.linux.aarch64.env
  artifacts:
    paths: [bin]
    reports:
      dotenv: artifacts.linux.aarch64.env

.spread:
  interruptible: true
  tags: [linux, docker, x86_64, kvm]
  stage: integration
  image: zyga/spread:latest
  variables:
    DEBIAN_FRONTEND: noninteractive
    SPREAD_QEMU_KVM: "1"
    TRANSFER_METER_FREQUENCY: "2s"
    ARTIFACT_COMPRESSION_LEVEL: "fast"
    CACHE_COMPRESSION_LEVEL: "fastest"
  parallel:
    matrix:
      - VERSION_CODENAME: [jammy]
        ID: [ubuntu]
        ID_VERSION: ["22.04"]
  before_script:
    - mkdir -p .cache
    - mkdir -p ~/.spread/qemu
    - |
      set -ex
      if [ ! -e .cache/autopkgtest-$VERSION_CODENAME-amd64.img ]; then
        # Prepare a qemu image that is useful for testing. For now this is just
        # a vanilla Ubuntu image with some packages pre-installed to speed up
        # the project prepare step in spread.yaml.
        cd .cache
        apt-get update && apt-get install -y autopkgtest qemu-utils genisoimage ca-certificates python3-distro-info
        time autopkgtest-buildvm-ubuntu-cloud --verbose -r $VERSION_CODENAME --post-command "apt-get -y install golang-go zmk docker.io docker-compose jq tofrodos git && docker pull rabbitmq:3-management && docker pull mysql:5.7 && docker pull zyga/hawkbit-snapshot:latest"
        cd ..
      fi
    - ln -s $CI_PROJECT_DIR/.cache/autopkgtest-$VERSION_CODENAME-amd64.img ~/.spread/qemu/$ID-$ID_VERSION-64.img
  script:
    - ':'
  cache:
    # If the image needs to be rebuilt, just bump this number.
    key: autopkgtest-image-$VERSION_CODENAME-3
    # The image compresses well. Don't compress it here, GitLab will compress
    # it automatically as a part of the cache creation step.
    paths:
     - .cache/autopkgtest-$VERSION_CODENAME-amd64.img

spread-cache:
  extends: .spread
  rules:
    - when: never
    - if: '$CI_PIPELINE_SOURCE == "schedule"'

spread/linux/x86_64:
  extends: .spread
  needs:
    - job: spread-cache
      optional: true
    - job: build/linux/x86_64
      artifacts: true
  script:
    # The prepare script in spread.yaml will use prebuilt/hawkbitctl if one exists.
    - mv bin/linux_amd64/ prebuilt/
    - 'time spread -v qemu:$ID-$ID_VERSION-64:'
  cache:
    policy: pull
  rules:
    - when: never

.docker:
  interruptible: true
  image: docker:latest
  stage: deploy
  services:
    - docker:dind
  before_script:
    - 'test -n "$CI_REGISTRY" || (
        echo "precondition failed - CI_REGISTRY is unset."
        && echo "It must be set the scheme-less URL of the registry (e.g. docker.io)."
        && exit 1)'
    - 'test -n "$CI_REGISTRY_USER" || (
        echo "precondition failed - CI_REGISTRY_USER is unset."
        && echo "It must be set to the docker registry account name."
        && exit 1)'
    - 'test -n "$CI_REGISTRY_PASSWORD_FILE" || (
        echo "precondition failed - CI_REGISTRY_PASSWORD_FILE is unset."
        && echo "It must be set to the name of a file with the registry token/password."
        && exit 1)'
    - 'test -f "$CI_REGISTRY_PASSWORD_FILE" || (
        echo "precondition failed - CI_REGISTRY_PASSWORD_FILE is not a file name."
        && echo "Inside GitLab CI/CD settings, make sure that the variable is of type *file*."
        && exit 1)'
    - docker login -u "$CI_REGISTRY_USER" --password-stdin "$CI_REGISTRY" < "$CI_REGISTRY_PASSWORD_FILE"
    - |
      if [ -n "$CI_COMMIT_TAG" ]; then
        CI_REGISTRY_TAG="$CI_COMMIT_TAG"
      elif [ "$CI_COMMIT_BRANCH" = "$CI_DEFAULT_BRANCH" ]; then
        CI_REGISTRY_TAG=latest
      else
        CI_REGISTRY_TAG="$CI_COMMIT_REF_SLUG"
      fi
      echo "$CI_REGISTRY_TAG" > CI_REGISTRY_TAG.env
  rules:
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH

docker-arch:
  extends: .docker
  tags: [linux, $ARCH, docker, dind]
  # Depend on both binaries, this is wasteful but also the only way to do this
  # without abandoning the parallel-matrix system.
  needs:
    - build/linux/x86_64
    - build/linux/aarch64
  parallel:
    matrix:
      - ARCH: x86_64
        GOARCH: amd64
      - ARCH: aarch64
        GOARCH: arm64
  script:
    - 'test -n "${CI_REGISTRY_IMAGE:-}" || (
        echo "precondition failed - CI_REGISTRY_IMAGE is not set."
        && echo "It must be set to the name of the image in the registry, including the username"
        && exit 1 )'
    - CI_REGISTRY_TAG="$(cat CI_REGISTRY_TAG.env)"
    # Dockerfile will use prebuilt/hawkbitctl if one exists.
    - mv bin/linux_${GOARCH}/ prebuilt/
    - docker build
      --pull
      --tag "${CI_REGISTRY_IMAGE}:${CI_REGISTRY_TAG}-${ARCH}"
      --build-arg "ARCH=$ARCH"
      .
    - docker build
      --pull
      --tag "${CI_REGISTRY_IMAGE}:${CI_REGISTRY_TAG}-ubuntu-${ARCH}"
      --target ubuntu
      --build-arg "ARCH=$ARCH"
      .
    - docker build
      --pull
      --tag "${CI_REGISTRY_IMAGE}:${CI_REGISTRY_TAG}-alpine-${ARCH}"
      --target alpine
      --build-arg "ARCH=$ARCH"
      .
    - docker save
      --output "${CI_REGISTRY_IMAGE/\//_}:${CI_REGISTRY_TAG}-${ARCH}.tar"
      "${CI_REGISTRY_IMAGE}:${CI_REGISTRY_TAG}-${ARCH}"
    - docker save
      --output "${CI_REGISTRY_IMAGE/\//_}:${CI_REGISTRY_TAG}-ubuntu-${ARCH}.tar"
      "${CI_REGISTRY_IMAGE}:${CI_REGISTRY_TAG}-ubuntu-${ARCH}"
    - docker save
      --output "${CI_REGISTRY_IMAGE/\//_}:${CI_REGISTRY_TAG}-alpine-${ARCH}.tar"
      "${CI_REGISTRY_IMAGE}:${CI_REGISTRY_TAG}-alpine-${ARCH}"
  artifacts:
    paths:
      - "*.tar"

docker-manifest:
  extends: .docker
  tags: [linux, x86_64, docker, dind]
  needs: [docker-arch]
  dependencies: [docker-arch]
  variables:
    GIT_STRATEGY: none
  script:
    - docker load --input "${CI_REGISTRY_IMAGE/\//_}:${CI_REGISTRY_TAG}-x86_64.tar"
    - docker load --input "${CI_REGISTRY_IMAGE/\//_}:${CI_REGISTRY_TAG}-ubuntu-x86_64.tar"
    - docker load --input "${CI_REGISTRY_IMAGE/\//_}:${CI_REGISTRY_TAG}-alpine-x86_64.tar"
    - docker load --input "${CI_REGISTRY_IMAGE/\//_}:${CI_REGISTRY_TAG}-aarch64.tar"
    - docker load --input "${CI_REGISTRY_IMAGE/\//_}:${CI_REGISTRY_TAG}-ubuntu-aarch64.tar"
    - docker load --input "${CI_REGISTRY_IMAGE/\//_}:${CI_REGISTRY_TAG}-alpine-aarch64.tar"

    - docker images

    - docker push "${CI_REGISTRY_IMAGE}:${CI_REGISTRY_TAG}-x86_64"
    - docker push "${CI_REGISTRY_IMAGE}:${CI_REGISTRY_TAG}-ubuntu-x86_64"
    - docker push "${CI_REGISTRY_IMAGE}:${CI_REGISTRY_TAG}-alpine-x86_64"
    - docker push "${CI_REGISTRY_IMAGE}:${CI_REGISTRY_TAG}-aarch64"
    - docker push "${CI_REGISTRY_IMAGE}:${CI_REGISTRY_TAG}-ubuntu-aarch64"
    - docker push "${CI_REGISTRY_IMAGE}:${CI_REGISTRY_TAG}-alpine-aarch64"

    - docker manifest create
        "${CI_REGISTRY_IMAGE}:${CI_REGISTRY_TAG}"
        --amend "${CI_REGISTRY_IMAGE}:${CI_REGISTRY_TAG}-x86_64"
        --amend "${CI_REGISTRY_IMAGE}:${CI_REGISTRY_TAG}-aarch64"
    - docker manifest create
        "${CI_REGISTRY_IMAGE}:${CI_REGISTRY_TAG}-ubuntu"
        --amend "${CI_REGISTRY_IMAGE}:${CI_REGISTRY_TAG}-ubuntu-x86_64"
        --amend "${CI_REGISTRY_IMAGE}:${CI_REGISTRY_TAG}-ubuntu-aarch64"
    - docker manifest create
        "${CI_REGISTRY_IMAGE}:${CI_REGISTRY_TAG}-alpine"
        --amend "${CI_REGISTRY_IMAGE}:${CI_REGISTRY_TAG}-alpine-x86_64"
        --amend "${CI_REGISTRY_IMAGE}:${CI_REGISTRY_TAG}-alpine-aarch64"

    - docker manifest push --purge "${CI_REGISTRY_IMAGE}:${CI_REGISTRY_TAG}"
    - docker manifest push --purge "${CI_REGISTRY_IMAGE}:${CI_REGISTRY_TAG}-ubuntu"
    - docker manifest push --purge "${CI_REGISTRY_IMAGE}:${CI_REGISTRY_TAG}-alpine"

# Cross-build all the combinations after all the tests pass.
cross-build:
  stage: deploy
  needs: null
  parallel:
    matrix:
      - GOOS: linux
        # Do not duplicate builds that are handled explicitly earlier.
        GOARCH: ["386", arm, riscv64]
        CI_GO_VERSION: ["1.18", "latest"]
      - GOOS: windows
        GOARCH: [amd64, arm64]
        CI_GO_VERSION: ["1.18", "latest"]
      - GOOS: darwin
        GOARCH: [amd64, arm64]
        CI_GO_VERSION: ["1.18", "latest"]

release:
  stage: deploy
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  tags: [linux, docker, x86_64]
  needs:
    - job: build/linux/x86_64
      artifacts: true
    - job: build/linux/aarch64
      artifacts: true
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - echo "$LINUX_X86_64_CI_JOB_ID"
    - echo "$LINUX_AARCH_CI_JOB_ID"
  release:
    name: 'HawkBit control tool built from $CI_COMMIT_TAG'
    description: "$CI_COMMIT_TAG"
    tag_name: "$CI_COMMIT_TAG"
    assets:
      links:
        - name: "hawkbitctl (Linux, x86_64)"
          url: 'https://gitlab.com/zygoon/go-hawkbit/-/jobs/${LINUX_X86_64_CI_JOB_ID}/artifacts/file/bin/linux_amd64/hawkbitctl'
        - name: "hawkbitctl (Linux, aarch64)"
          url: 'https://gitlab.com/zygoon/go-hawkbit/-/jobs/${LINUX_AARCH64_CI_JOB_ID}/artifacts/file/bin/linux_arm64/hawkbitctl'

earthly:
  image: earthly/earthly:v0.7.5
  services:
    - docker:dind
  tags: [linux, docker, dind, x86_64]
  stage: build
  variables:
    DOCKER_HOST: tcp://docker:2375
    FORCE_COLOR: 1
    EARTHLY_EXEC_CMD: "/bin/sh"
  before_script:
    - earthly bootstrap
  script:
    - earthly --ci +build
