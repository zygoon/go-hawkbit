# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

summary: GET /rest/v1/targets/1001 resulting in 200
prepare: |
    # Create a target we will interrogate.
    curl \
        --verbose \
        -H "$TEST_HAWKBIT_AUTH" \
        -H 'Accept: application/hal+json' \
        -H 'Content-Type: application/json;charset=UTF-8' \
        "${TEST_HAWKBIT_URL}/rest/v1/targets" \
        -d '[{"controllerId": "1001", "name": "Test Device 1001",  "description": "First test device", "securityToken": "9999", "address": "https://localhost:8001"}]'
execute: |
    # We can retrieve a specific target programmatically.
    # https://www.eclipse.org/hawkbit/rest-api/targets-api-guide/#_get_rest_v1_targets_targetid
    curl \
        --verbose \
        --dump-header resp.head \
        --output resp.body \
        -H "$TEST_HAWKBIT_AUTH" \
        -H 'Accept: application/hal+json' \
        -H 'Content-Type: application/json;charset=UTF-8' \
        "${TEST_HAWKBIT_URL}/rest/v1/targets/1001" \

    # The request succeeds.
    fromdos resp.head
    GREP -qFx 'HTTP/1.1 200 OK' < resp.head

    # The returned data is sane.
    jq '.createdBy' -r < resp.body | GREP -qFx admin
    jq '.lastModifiedBy' -r < resp.body | GREP -qFx admin
    jq '.name' -r < resp.body | GREP -qFx "Test Device 1001"
    jq '.description' -r < resp.body | GREP -qFx "First test device"
    jq '.controllerId' -r < resp.body | GREP -qFx 1001
    jq '.updateStatus' -r < resp.body | GREP -qFx unknown
    jq '.ipAddress' -r < resp.body | GREP -qFx localhost
    jq '.address' -r < resp.body | GREP -qFx https://localhost:8001
    jq '.securityToken' -r < resp.body | GREP -qFx 9999
    jq '.requestAttributes' -r < resp.body | GREP -qFx true
    jq '._links.self.href' -r < resp.body | GREP -qFx "${TEST_HAWKBIT_URL}/rest/v1/targets/1001"
    jq '._links.assignedDS.href' -r < resp.body | GREP -qFx "${TEST_HAWKBIT_URL}/rest/v1/targets/1001/assignedDS"
    jq '._links.installedDS.href' -r < resp.body | GREP -qFx "${TEST_HAWKBIT_URL}/rest/v1/targets/1001/installedDS"
    jq '._links.attributes.href' -r < resp.body | GREP -qFx "${TEST_HAWKBIT_URL}/rest/v1/targets/1001/attributes"
    jq '._links.actions.href' -r < resp.body | GREP -qFx "${TEST_HAWKBIT_URL}/rest/v1/targets/1001/actions?offset=0&limit=50&sort=id:DESC"
    jq '._links.metadata.href' -r < resp.body | GREP -qFx "${TEST_HAWKBIT_URL}/rest/v1/targets/1001/metadata?offset=0&limit=50{&sort,q}"
    jq '._links.metadata.templated' -r < resp.body | GREP -qFx true
restore: |
    # Delete the target.
    curl \
        --verbose \
        --include \
        -H "$TEST_HAWKBIT_AUTH" \
        -H 'Accept: application/hal+json' \
        "${TEST_HAWKBIT_URL}/rest/v1/targets/1001" -X DELETE

