# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

summary: POST /rest/v1/distributionsets resulting in 400

prepare: |
    # This test references what must be created in advance of the excute command. We need to define a
    # distribution-set-type and a distribution-set

    curl \
        --verbose \
        --dump-header resp.head \
        --output resp.body \
        -H "$TEST_HAWKBIT_AUTH" \
        -H 'Accept: application/hal+json' \
        -H 'Content-Type: application/json;charset=UTF-8' \
        "${TEST_HAWKBIT_URL}/rest/v1/distributionsettypes" \
        -d '[{"name": "Oniro Distribution-type", "description":"Oniro Distribution description", "key": "oniro"}]'

    fromdos resp.head
    GREP -qFx 'HTTP/1.1 201 Created' <resp.head
    GREP -qFx 'Content-Type: application/hal+json;charset=utf-8' <resp.head
    printf 'DS_TYPE_ID="%s"\n' "$(jq -r '.[0].id' <resp.body)" >>vars.sh
    
    #create the first distribution-set
    curl \
        --verbose \
        --dump-header resp.head \
        --output resp.body \
        -H "$TEST_HAWKBIT_AUTH" \
        -H 'Accept: application/hal+json' \
        -H 'Content-Type: application/json;charset=UTF-8' \
        "${TEST_HAWKBIT_URL}/rest/v1/distributionsets" \
        -d '[{"name": "Oniro Distribution", "description":"Oniro Distribution description", "type": "oniro","version":"one"}]'

    fromdos resp.head
    GREP -qFx 'HTTP/1.1 201 Created' <resp.head
    GREP -qFx 'Content-Type: application/hal+json;charset=utf-8' <resp.head
    printf 'DS_ID="%s"\n' "$(jq -r '.[0].id' <resp.body)" >>vars.sh

excute: |
    # We cannot create a distributionset with a duplicated name and version
    # https://www.eclipse.org/hawkbit/rest-api/distributionsets-api-guide/#_post_rest_v1_distributionsets

    curl \
        --verbose \
        --dump-header resp.head \
        --output resp.body \
        -H "$TEST_HAWKBIT_AUTH" \
        -H 'Accept: application/hal+json' \
        -H 'Content-Type: application/json;charset=UTF-8' \
        "${TEST_HAWKBIT_URL}/rest/v1/distributionsets" \
        -d '[{"name": "Oniro Distribution", "description":"Oniro Distribution description", "type": "oniro","version":"one"}]'

    # The returned exception is sane.
    jq '.exceptionClass' -r < resp.body | GREP -qFx org.eclipse.hawkbit.repository.exception.EntityAlreadyExistsException
    jq '.errorCode' -r < resp.body | GREP -qFx hawkbit.server.error.repo.entitiyAlreayExists
    jq '.message' -r < resp.body | GREP -qFx "The given entity already exists in database"

restore: |
 . vars.sh

    # Delete the distribution-set we've created earlier.
    if [ -n "${DS_ID:-}" ]; then
        echo "Deleting distribution-set with ID=$DS_ID"
        curl \
            --verbose \
            --dump-header resp.head \
            --output resp.body \
            -H "$TEST_HAWKBIT_AUTH" \
            -H 'Accept: application/hal+json' \
            "${TEST_HAWKBIT_URL}/rest/v1/distributionsets/${DS_ID}" -X DELETE

        # The request succeeds.
        fromdos resp.head
        GREP -qFx 'HTTP/1.1 200 OK' <resp.head

        # Is it deleted, or soft-deleted? We want really deleted here.
        curl \
            --verbose \
            --dump-header resp.head \
            --output resp.body \
            -H "$TEST_HAWKBIT_AUTH" \
            -H 'Accept: application/hal+json' \
            "${TEST_HAWKBIT_URL}/rest/v1/distributionsets/${DS_ID}"

        # The request succeeds.
        fromdos resp.head
        GREP -qFx 'HTTP/1.1 404 Not Found' <resp.head
    fi

    # Delete the distribution-set-type we've created earlier.
    if [ -n "${DS_TYPE_ID:-}" ]; then
        echo "Deleting distribution-set-type with ID=$DS_TYPE_ID"
        curl \
            --verbose \
            --dump-header resp.head \
            --output resp.body \
            -H "$TEST_HAWKBIT_AUTH" \
            -H 'Accept: application/hal+json' \
            "${TEST_HAWKBIT_URL}/rest/v1/distributionsettypes/${DS_TYPE_ID}" -X DELETE

        # The request succeeds.
        fromdos resp.head
        GREP -qFx 'HTTP/1.1 200 OK' <resp.head

        # Is it deleted, or soft-deleted? We want really deleted here.
        curl \
            --verbose \
            --dump-header resp.head \
            --output resp.body \
            -H "$TEST_HAWKBIT_AUTH" \
            -H 'Accept: application/hal+json' \
            "${TEST_HAWKBIT_URL}/rest/v1/distributionsettypes/${DS_TYPE_ID}"

        # The request succeeds.
        fromdos resp.head
        GREP -qFx 'HTTP/1.1 404 Not Found' <resp.head
    fi
    
    rm -f vars.sh