# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

summary: DELETE /rest/v1/distributionsets resulting in 200

prepare: |
    # We will be collecting variables as we execute.
    echo >vars.sh
    # This test references what must be created in advance of the excute command. We need to define a
    # distributionsets-type, and a distribution-set .
    
    curl \
        --verbose \
        --dump-header resp.head \
        --output resp.body \
        -H "$TEST_HAWKBIT_AUTH" \
        -H 'Accept: application/hal+json' \
        -H 'Content-Type: application/json;charset=UTF-8' \
        "${TEST_HAWKBIT_URL}/rest/v1/distributionsettypes" \
        -d '[{"name": "Oniro Distribution-type", "description":"Oniro Distribution description", "key": "oniro"}]'

    fromdos resp.head
    GREP -qFx 'HTTP/1.1 201 Created' <resp.head
    GREP -qFx 'Content-Type: application/hal+json;charset=utf-8' <resp.head
    printf 'DS_TYPE_ID="%s"\n' "$(jq -r '.[0].id' <resp.body)" >>vars.sh
    
    # create distribution-set
    curl \
        --verbose \
        --dump-header resp.head \
        --output resp.body \
        -H "$TEST_HAWKBIT_AUTH" \
        -H 'Accept: application/hal+json' \
        -H 'Content-Type: application/json;charset=UTF-8' \
        "${TEST_HAWKBIT_URL}/rest/v1/distributionsets" \
        -d '[{"name": "Oniro Distribution", "description":"Oniro Distribution description", "type": "oniro","version":"one"}]'

    # The request succeeds.
    fromdos resp.head
    GREP -qFx 'HTTP/1.1 201 Created' <resp.head
    GREP -qFx 'Content-Type: application/hal+json;charset=utf-8' <resp.head
    printf 'DS_ID="%s"\n' "$(jq -r '.[0].id' <resp.body)" >>vars.sh

execute: |
    . vars.sh

    # Delete the Distribution-set.
    curl \
        --verbose \
        --dump-header resp.head \
        --output resp.body \
        -H "$TEST_HAWKBIT_AUTH" \
        -H 'Accept: application/hal+json' \
        "${TEST_HAWKBIT_URL}/rest/v1/distributionsets/$DS_ID" -X DELETE

    # The request succeeds.
    fromdos resp.head
    GREP -qFx 'HTTP/1.1 200 OK' < resp.head

    # The body is empty.
    test 0 -eq "$(stat -c %s resp.body)"

restore: |
    . vars.sh

    # Delete the distribution-set-type we've created earlier.
    if [ -n "${DS_TYPE_ID:-}" ]; then
        echo "Deleting distribution-set-type with ID=$DS_TYPE_ID"
        curl \
            --verbose \
            --dump-header resp.head \
            --output resp.body \
            -H "$TEST_HAWKBIT_AUTH" \
            -H 'Accept: application/hal+json' \
            "${TEST_HAWKBIT_URL}/rest/v1/distributionsettypes/${DS_TYPE_ID}" -X DELETE

        # The request succeeds.
        fromdos resp.head
        GREP -qFx 'HTTP/1.1 200 OK' <resp.head

        # Is it deleted, or soft-deleted? We want really deleted here.
        curl \
            --verbose \
            --dump-header resp.head \
            --output resp.body \
            -H "$TEST_HAWKBIT_AUTH" \
            -H 'Accept: application/hal+json' \
            "${TEST_HAWKBIT_URL}/rest/v1/distributionsettypes/${DS_TYPE_ID}"

        # The request succeeds.
        fromdos resp.head
        GREP -qFx 'HTTP/1.1 404 Not Found' <resp.head
    fi

    rm -f vars.sh