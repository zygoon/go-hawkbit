# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

summary: DELETE /rest/v1/distributionsets/1001 resulting in 404

execute: |
    # We cannot delete a distribution-set which does not exist.
    # https://www.eclipse.org/hawkbit/rest-api/distributionsets-api-guide/#_delete_rest_v1_distributionsets_sdistributionsetsid
    curl \
        --verbose \
        --dump-header resp.head \
        --output resp.body \
        -H "$TEST_HAWKBIT_AUTH" \
        -H 'Accept: application/hal+json' \
        "${TEST_HAWKBIT_URL}/rest/v1/distributionsets/1001" -X DELETE

    # The request fails.
    fromdos resp.head
    GREP -qFx 'HTTP/1.1 404 Not Found' < resp.head

    # The returned exception information is sane.
    jq '.exceptionClass' -r < resp.body | GREP -qFx "org.eclipse.hawkbit.repository.exception.EntityNotFoundException"
    # Yes, the response contains a typo.
    # This is issue https://github.com/eclipse/hawkbit/issues/1236
    jq '.errorCode' -r < resp.body | GREP -qFx "hawkbit.server.error.repo.entitiyNotFound"
    jq '.message' -r < resp.body | GREP -qFx "DistributionSet with given identifier {1001} does not exist."