<!--
SPDX-License-Identifier: Apache-2.0
SPDX-FileCopyrightText: Huawei Inc.
-->

![Build status](https://img.shields.io/gitlab/pipeline-status/zygoon/go-hawkbit?branch=master)
# About the Go HawkBit module

This module provides incomplete, under-development Go bindings to the HawkBit
APIs, with special focus on the management APIs.

## The hawkbitctl tool

The `hawkbitctl` tool exposes most of the implemented bindings. The tool is
intended for automation, integration into continuous integration workflows and
other forms of testing.

**WARNING** The precise command interface is not yet stable and may see small
renames and consistency tweaks before the first release.

### Snap package

[![hawkbitctl](https://snapcraft.io/hawkbitctl/badge.svg)](https://snapcraft.io/hawkbitctl)

You can install `hawkbitctl` from the snap store. Click on the image for instructions.

[![Get it from the Snap Store](https://snapcraft.io/static/images/badges/en/snap-store-white.svg)](https://snapcraft.io/hawkbitctl)

### Docker containers

You can run `hawkbitctl` from the official docker containers, published on DockerHub
at https://hub.docker.com/r/zyga/hawkbitctl.

There are three containers available:
 - `zyga/hawkbitctl:latest`
 - `zyga/hawkbitctl:latest-alpine`
 - `zyga/hawkbitctl:latest-ubuntu`

The first container is particularly small, as it contains just the `hawkbitctl`
executable. For CI/CD environments, you may want to use the Alpine or
Ubuntu-based containers, that also offer shell and access to rich package
repositories.

## Contributions

Contributions are welcome. Please try to respect Go requirements (1.18 at the
moment) and the overall coding and testing style.

## License and REUSE

This project is licensed under the Apache 2.0 license, see the `LICENSE` file
for details. The project is compliant with https://reuse.software/ - making it
easy to ensure license and copyright compliance by automating software
bill-of-materials. In other words, it's a good citizen in the modern free
software stacks.
