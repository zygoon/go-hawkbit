# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

VERSION 0.7
FROM golang:latest

WORKDIR /src

deps:
    COPY go.mod go.sum ./
    RUN go mod download
    SAVE ARTIFACT go.mod AS LOCAL go.mod
    SAVE ARTIFACT go.sum AS LOCAL go.sum

build:
    FROM +deps
    COPY . .
    RUN go mod download
    RUN CGO_ENABLED=0 go build \
        -ldflags=-s -ldflags=-w -trimpath \
        -o hawkbitctl \
        ./cmd/hawkbitctl
    SAVE ARTIFACT hawkbitctl AS LOCAL hawkbitctl

docker:
    FROM alpine:latest
    COPY +build/hawkbitctl /usr/local/bin/
    ENTRYPOINT ["/usr/local/bin/hawkbitctl"]
    SAVE IMAGE hawkbitctl:latest
