// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package adapters

const (
	fmtFields2        = "%v\t%v\n"
	fmtFields3        = "%v\t%v\t%v\n"
	fmtFields4        = "%v\t%v\t%v\t%v\n"
	fmtFields5        = "%v\t%v\t%v\t%v\t%v\n"
	fmtFields7        = "%v\t%v\t%v\t%v\t%v\t%v\t%v\n"
	fmtInsetKeyValue  = "    %s: %v\n"
	fmtKeyList        = "%s: %d item(s)\n"
	fmtKeyValue       = "%s: %v\n"
	attrKey           = "Key"
	attrValue         = "Value"
	attrTargetVisible = "Target Visible"
)
