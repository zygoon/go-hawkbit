// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package adapters

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"io"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/swmodtypes"
	"gitlab.com/zygoon/go-hawkbit/pkg/restapi"
)

const (
	swModTypeIdHelp   = "Software module type ID"
	cmdNameSwModType  = "software-module-type"
	cmdNameSwModTypes = "software-module-types"
)

var (
	errSwModTypeIdRequired   = errors.New("software module type ID required")
	errSwModTypeNameRequired = errors.New("software module type name required")
	errSwModTypeKeyRequired  = errors.New("software module type key required")
)

// SoftwareModuleType is the hawkbitctl adapter for mgmtapi.SoftwareModuleType type.
type SoftwareModuleType struct{}

func (SoftwareModuleType) SingularName() string {
	return cmdNameSwModType
}

func (SoftwareModuleType) PluralName() string {
	return cmdNameSwModTypes
}

func (SoftwareModuleType) KnownFields() []string {
	return []string{
		jsID,
		jsKey,
		jsName,
		jsDescription,

		jsCreatedAt,
		jsCreatedBy,
		jsLastModifiedAt,
		jsLastModifiedBy,
		jsDeleted,
	}
}

func (SoftwareModuleType) AddCreateFlags(fs *flag.FlagSet, c *swmodtypes.CreateData) {
	fs.StringVar(&c.Key, argKey, "", "Alternate identifier of the new software module type")
	fs.StringVar(&c.Name, argName, "", "Name of the new software module type")
	fs.StringVar(&c.Description, argDescription, "", "Description of the new software module type")
	fs.IntVar(&c.MaxAssignments, argMaxAssignments, 0, "Maximum number of assignments of the new software module type")
}

func (SoftwareModuleType) AddSelectFlags(fs *flag.FlagSet, id *swmodtypes.ID) {
	fs.Var(id, argID, swModTypeIdHelp)
}

func (SoftwareModuleType) AddUpdateFlags(fs *flag.FlagSet, u *swmodtypes.UpdateData) {
	fs.Var(optString{ValPtrPtr: &u.Description}, argDescription, "Updated description of the software module type")
}

func (SoftwareModuleType) CanCreate(c *swmodtypes.CreateData) error {
	if c.Name == "" {
		return errSwModTypeNameRequired
	}

	if c.Key == "" {
		return errSwModTypeKeyRequired
	}

	return nil
}

func (SoftwareModuleType) CanSelect(id swmodtypes.ID) error {
	if id == 0 {
		return errSwModTypeIdRequired
	}

	return nil
}

func (SoftwareModuleType) Create(ctx context.Context, cli *mgmtapi.Client, cs []swmodtypes.CreateData) ([]swmodtypes.Data, error) {
	return cli.SoftwareModuleTypeModel().Create(ctx, cli.Client, cs)
}

func (SoftwareModuleType) Delete(ctx context.Context, cli *mgmtapi.Client, id swmodtypes.ID) error {
	return cli.SoftwareModuleTypeModel().Ref(id).Delete(ctx, cli.Client)
}

func (SoftwareModuleType) Get(ctx context.Context, cli *mgmtapi.Client, id swmodtypes.ID) (*swmodtypes.Data, error) {
	return cli.SoftwareModuleTypeModel().Ref(id).Get(ctx, cli.Client)
}

func (SoftwareModuleType) Update(ctx context.Context, cli *mgmtapi.Client, id swmodtypes.ID, up *swmodtypes.UpdateData) (*swmodtypes.Data, error) {
	return cli.SoftwareModuleTypeModel().Ref(id).Update(ctx, cli.Client, up)
}

func (SoftwareModuleType) Show(ctx context.Context, cli *mgmtapi.Client, r *swmodtypes.Data) error {
	_, w, _ := cmdr.Stdio(ctx)

	_, _ = fmt.Fprintf(w, fmtKeyValue, fldID, r.ID)
	_, _ = fmt.Fprintf(w, fmtKeyValue, fldKey, r.Key)
	_, _ = fmt.Fprintf(w, fmtKeyValue, fldName, r.Name)
	_, _ = fmt.Fprintf(w, fmtKeyValue, fldMaxAssignments, r.MaxAssignments)
	_, _ = fmt.Fprintf(w, fmtKeyValue, fldDeleted, r.Deleted)
	_, _ = fmt.Fprintf(w, fmtKeyValue, fldDescription, r.Description)

	return nil
}

func (SoftwareModuleType) Find(ctx context.Context, cli *mgmtapi.Client, opts restapi.FindOptions) (*restapi.Found[swmodtypes.FindData], error) {
	return cli.SoftwareModuleTypeModel().Find(ctx, cli.Client, opts)
}

func (SoftwareModuleType) ListHeader(w io.Writer) error {
	_, err := fmt.Fprintf(w, fmtFields4, fldID, fldKey, fldName, fldMaxAssignments)

	return err
}

func (SoftwareModuleType) ListItem(w io.Writer, f *swmodtypes.FindData) error {
	_, err := fmt.Fprintf(w, fmtFields4, f.ID, f.Key, f.Name, f.MaxAssignments)

	return err
}
