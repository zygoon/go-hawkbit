// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package adapters

import (
	"strconv"
	"strings"

	"gitlab.com/zygoon/go-hawkbit/pkg/restapi"
)

// IdRefListValue is a list of references to foreign keys.
//
// IdRefListValue implements the flag.Value interface and acts as a value type to be
// used with flag.FlagSet.Var(). The state is maintained by modifying the slice
// pointed to by IdRefListPtr.
type IdRefListValue[T interface{ ~int }, mapEnvT any] struct {
	IdRefListPtr *[]restapi.IdRef[T]
	Mapper       IdMapper[T, string, mapEnvT]
	Env          mapEnvT
}

// String returns a comma-separated list of identifiers.
//
// If the mapper is available it is consulted to translate identifier
// references to key references. Identifiers which cannot be resolved
// are left as-is.
func (v IdRefListValue[T, mapEnvT]) String() string {
	if v.IdRefListPtr == nil {
		return ""
	}

	var b strings.Builder

	for idx, idRef := range *v.IdRefListPtr {
		if idx > 0 {
			b.WriteByte(',')
		}

		var s string

		if v.Mapper != nil {
			if alt, err := v.Mapper.ToAlternate(idRef.ID, v.Env); err == nil {
				s = alt
			}
		}

		if s == "" {
			s = strconv.Itoa(int(idRef.ID))
		}

		b.WriteString(s)
	}

	return b.String()
}

// Set parses a list of comma-separated identifiers.
//
// The list may use IDs (integers) or keys (strings) interchangeably. Numeric
// identifiers take priority over non-numeric keys. If a non-numeric key does not
// map to a known identifier the assignment fails.
func (v IdRefListValue[T, mapEnvT]) Set(s string) error {
	fields := strings.FieldsFunc(s, func(r rune) bool { return r == ',' })
	idRefList := make([]restapi.IdRef[T], 0, len(fields))

	for _, f := range fields {
		var idRef restapi.IdRef[T]

		id, err := strconv.Atoi(f)
		if err == nil {
			idRef.ID = T(id)
		} else if v.Mapper != nil {
			idRef.ID, err = v.Mapper.ToPrimary(f, v.Env)
		}

		if err != nil {
			return err
		}

		idRefList = append(idRefList, idRef)
	}

	*v.IdRefListPtr = idRefList

	return nil
}
