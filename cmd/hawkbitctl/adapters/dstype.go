// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package adapters

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"io"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/dstypes"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/swmodtypes"
	"gitlab.com/zygoon/go-hawkbit/pkg/restapi"
)

const (
	dsTypeIdHelp   = "Distribution set type ID"
	cmdNameDsType  = "distribution-set-type"
	cmdNameDsTypes = "distribution-set-types"
)

var (
	errDsTypeIdRequired   = errors.New("distribution set type ID required")
	errDsTypeNameRequired = errors.New("distribution set type name required")
	errDsTypeKeyRequired  = errors.New("distribution set type key required")
)

// DistributionSetType is the hawkbitctl adapter for distribution set types.
type DistributionSetType struct {
	SwModTypeMapper IdMapper[swmodtypes.ID, string, struct{}]
}

func (DistributionSetType) SingularName() string {
	return cmdNameDsType
}

func (DistributionSetType) PluralName() string {
	return cmdNameDsTypes
}

func (DistributionSetType) KnownFields() []string {
	return []string{
		jsID,
		jsKey,
		jsName,
		jsDescription,

		jsCreatedAt,
		jsCreatedBy,
		jsLastModifiedAt,
		jsLastModifiedBy,
		jsDeleted,
	}
}

func (a DistributionSetType) AddCreateFlags(fs *flag.FlagSet, c *dstypes.CreateData) {
	fs.StringVar(&c.Key, argKey, "", "Alternate identifier of the new distribution set type")
	fs.StringVar(&c.Name, argName, "", "Name of the new distribution set type")
	fs.StringVar(&c.Description, argDescription, "", "Description of the new distribution set type")
	fs.Var(IdRefListValue[swmodtypes.ID, struct{}]{IdRefListPtr: &c.MandatoryModules, Mapper: a.SwModTypeMapper}, argMandatoryModules, "List of mandatory software module types")
	fs.Var(IdRefListValue[swmodtypes.ID, struct{}]{IdRefListPtr: &c.MandatoryModules, Mapper: a.SwModTypeMapper}, argOptionalModules, "List of optional software module types")
}

func (DistributionSetType) AddUpdateFlags(fs *flag.FlagSet, u *dstypes.UpdateData) {
	fs.Var(optString{ValPtrPtr: &u.Description}, argDescription, "Updated description of the distribution set type")
}

func (DistributionSetType) AddSelectFlags(fs *flag.FlagSet, id *dstypes.ID) {
	fs.Var(id, argID, dsTypeIdHelp)
}

func (DistributionSetType) CanCreate(c *dstypes.CreateData) error {
	if c.Name == "" {
		return errDsTypeNameRequired
	}

	if c.Key == "" {
		return errDsTypeKeyRequired
	}

	return nil
}

func (DistributionSetType) CanSelect(id dstypes.ID) error {
	if id == 0 {
		return errDsTypeIdRequired
	}

	return nil
}

func (DistributionSetType) Create(ctx context.Context, cli *mgmtapi.Client, cs []dstypes.CreateData) ([]dstypes.Data, error) {
	return cli.DistributionSetTypeModel().Create(ctx, cli.Client, cs)
}

func (DistributionSetType) Delete(ctx context.Context, cli *mgmtapi.Client, id dstypes.ID) error {
	return cli.DistributionSetTypeModel().Ref(id).Delete(ctx, cli.Client)
}

func (DistributionSetType) Get(ctx context.Context, cli *mgmtapi.Client, id dstypes.ID) (*dstypes.Data, error) {
	return cli.DistributionSetTypeModel().Ref(id).Get(ctx, cli.Client)
}

func (DistributionSetType) Update(ctx context.Context, cli *mgmtapi.Client, id dstypes.ID, up *dstypes.UpdateData) (*dstypes.Data, error) {
	return cli.DistributionSetTypeModel().Ref(id).Update(ctx, cli.Client, up)
}

func (DistributionSetType) Show(ctx context.Context, cli *mgmtapi.Client, r *dstypes.Data) error {
	_, w, _ := cmdr.Stdio(ctx)

	mandatory, err := r.MandatoryModules().All(ctx, cli.Client)
	if err != nil {
		return err
	}

	optional, err := r.OptionalModules().All(ctx, cli.Client)
	if err != nil {
		return err
	}

	_, _ = fmt.Fprintf(w, fmtKeyValue, fldID, r.ID)
	_, _ = fmt.Fprintf(w, fmtKeyValue, fldKey, r.Key)
	_, _ = fmt.Fprintf(w, fmtKeyValue, fldName, r.Name)
	_, _ = fmt.Fprintf(w, fmtKeyValue, fldDeleted, r.Deleted)
	_, _ = fmt.Fprintf(w, fmtKeyValue, fldDescription, r.Description)

	_, _ = fmt.Fprintf(w, fmtKeyList, fldMandatorySoftwareModuleTypes, len(mandatory))
	for _, smt := range mandatory {
		_, _ = fmt.Fprintf(w, fmtInsetKeyValue, fldName, smt.Name)
		_, _ = fmt.Fprintf(w, fmtInsetKeyValue, fldMaxAssignments, smt.MaxAssignments)
	}

	_, _ = fmt.Fprintf(w, fmtKeyList, fldOptionalSoftwareModuleTypes, len(optional))
	for _, smt := range optional {
		_, _ = fmt.Fprintf(w, fmtInsetKeyValue, fldName, smt.Name)
		_, _ = fmt.Fprintf(w, fmtInsetKeyValue, fldMaxAssignments, smt.MaxAssignments)
	}

	return nil
}

func (DistributionSetType) Find(ctx context.Context, cli *mgmtapi.Client, opts restapi.FindOptions) (*restapi.Found[dstypes.FindData], error) {
	return cli.DistributionSetTypeModel().Find(ctx, cli.Client, opts)
}

func (DistributionSetType) ListHeader(w io.Writer) error {
	_, err := fmt.Fprintf(w, fmtFields3, fldID, fldKey, fldName)

	return err
}

func (DistributionSetType) ListItem(w io.Writer, f *dstypes.FindData) error {
	_, err := fmt.Fprintf(w, fmtFields3, f.ID, f.Key, f.Name)

	return err
}
