// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package adapters

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"io"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/artifacts"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/swmods"
)

const (
	artifactIdHelp   = "Artifact ID"
	cmdNameArtifact  = "artifact"
	cmdNameArtifacts = "artifacts"
)

var (
	errArtifactIdRequired = errors.New("artifact ID required")
	errArtifactRequired   = errors.New("artifact file required")
	errModuleRequired     = errors.New("software module reference required")
)

type ArtifactSelector struct {
	ModuleId   swmods.ID
	ArtifactId artifacts.ID
}

func (sel *ArtifactSelector) SoftwareModuleID() (swmods.ID, error) {
	if sel.ModuleId == 0 {
		return 0, errSwModIdRequired
	}

	return sel.ModuleId, nil
}

type Artifact struct {
	SwModMapper    IdMapper[swmods.ID, string, struct{}]
	ArtifactMapper IdMapper[artifacts.ID, string, *ArtifactSelector]
}

func (Artifact) SingularName() string {
	return cmdNameArtifact
}

func (Artifact) PluralName() string {
	return cmdNameArtifacts
}

type ArtifactListState struct {
	ModuleID swmods.ID
}

func (a Artifact) AddSelectFlags(fs *flag.FlagSet, sel *ArtifactSelector) {
	fs.Var(IdValue[swmods.ID, *swmods.ID, string, struct{}]{
		IdPtr:  &sel.ModuleId,
		Mapper: a.SwModMapper,
	}, "m", "Software module owning the artifact")
	fs.Var(IdValue[artifacts.ID, *artifacts.ID, string, *ArtifactSelector]{
		IdPtr:  &sel.ArtifactId,
		Mapper: a.ArtifactMapper,
		Env:    sel,
	}, "a", "Artifact ID")
}

func (a Artifact) AddGetAllFlags(fs *flag.FlagSet, state *ArtifactListState) {
	fs.Var(IdValue[swmods.ID, *swmods.ID, string, struct{}]{
		IdPtr:  &state.ModuleID,
		Mapper: a.SwModMapper,
	}, "m", "Software module owning the artifacts")
}

func (Artifact) CanSelect(sel ArtifactSelector) error {
	if sel.ModuleId == 0 {
		return errSwModIdRequired
	}

	if sel.ArtifactId == 0 {
		return errArtifactIdRequired
	}

	return nil
}

func (Artifact) CanGetAll(state *ArtifactListState) error {
	if state.ModuleID == 0 {
		return errSwModIdRequired
	}

	return nil
}

func (Artifact) Get(ctx context.Context, cli *mgmtapi.Client, sel ArtifactSelector) (*artifacts.Data, error) {
	return cli.SoftwareModuleModel().Ref(sel.ModuleId).ArtifactModel().Ref(sel.ArtifactId).Get(ctx, cli.Client)
}

func (Artifact) GetAll(ctx context.Context, cli *mgmtapi.Client, state *ArtifactListState) ([]artifacts.Data, error) {
	return cli.SoftwareModuleModel().Ref(state.ModuleID).ArtifactModel().GetAll(ctx, cli.Client)
}

func (Artifact) Delete(ctx context.Context, cli *mgmtapi.Client, sel ArtifactSelector) error {
	return cli.SoftwareModuleModel().Ref(sel.ModuleId).ArtifactModel().Ref(sel.ArtifactId).Delete(ctx, cli.Client)
}

func (Artifact) ListHeader(w io.Writer) error {
	_, err := fmt.Fprintf(w, fmtFields3, fldID, fldSize, fldName)

	return err
}

func (Artifact) ListItem(w io.Writer, f *artifacts.Data) error {
	_, err := fmt.Fprintf(w, fmtFields3, f.ID, f.Size, f.ProvidedFileName)

	return err
}

func (Artifact) Show(ctx context.Context, cli *mgmtapi.Client, r *artifacts.Data) error {
	_, w, _ := cmdr.Stdio(ctx)

	_, _ = fmt.Fprintf(w, fmtKeyValue, fldID, r.ID)
	_, _ = fmt.Fprintf(w, fmtKeyValue, fldName, r.ProvidedFileName)
	_, _ = fmt.Fprintf(w, fmtKeyValue, fldSize, r.Size)
	_, _ = fmt.Fprintf(w, fmtKeyValue, fldMd5Sum, r.Hashes.Md5Sum)
	_, _ = fmt.Fprintf(w, fmtKeyValue, fldSha1Sum, r.Hashes.Sha1Sum)
	_, _ = fmt.Fprintf(w, fmtKeyValue, fldSha256Sum, r.Hashes.Sha256Sum)

	return nil
}

type ArtifactUploadState struct {
	module   swmods.ID
	artifact string
}

func (a Artifact) AddUploadFlags(fs *flag.FlagSet, state *ArtifactUploadState) {
	fs.Var(IdValue[swmods.ID, *swmods.ID, string, struct{}]{
		IdPtr:  &state.module,
		Mapper: a.SwModMapper,
	}, "m", "Software module to reference")
	fs.StringVar(&state.artifact, "a", "", "Artifact to upload")
}

func (Artifact) CanUpload(state *ArtifactUploadState) error {
	if state.module == 0 {
		return errModuleRequired
	}

	if state.artifact == "" {
		return errArtifactRequired
	}

	return nil
}

func (Artifact) Upload(ctx context.Context, cli *mgmtapi.Client, state *ArtifactUploadState) (*artifacts.Data, error) {
	_, stdout, _ := cmdr.Stdio(ctx)

	_, _ = fmt.Fprintf(stdout, "Uploading %s to module %d ...\n", state.artifact, state.module)

	return cli.SoftwareModuleModel().Ref(state.module).ArtifactModel().Upload(ctx, cli.Client, state.artifact)
}
