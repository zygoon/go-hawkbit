// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package adapters_test

import (
	"fmt"
	"reflect"
	"testing"

	"gitlab.com/zygoon/go-hawkbit/cmd/hawkbitctl/adapters"
	"gitlab.com/zygoon/go-hawkbit/pkg/restapi"
)

type keyMap[T interface{ ~int }] map[string]T

func (m keyMap[T]) ToPrimary(alt string, env struct{}) (T, error) {
	if id, ok := m[alt]; ok {
		return id, nil
	}

	return 0, fmt.Errorf("reference to unknown key %s", alt)
}

func (m keyMap[T]) ToAlternate(id T, env struct{}) (string, error) {
	for alt, someID := range m {
		if someID == id {
			return alt, nil
		}
	}

	return "", fmt.Errorf("reference to unknown id %d", id)
}

func TestIdRefListRendering(t *testing.T) {
	l := []restapi.IdRef[int]{{ID: 42}, {ID: 7}}
	v := adapters.IdRefListValue[int, struct{}]{IdRefListPtr: &l}

	if s := v.String(); s != "42,7" {
		t.Fatalf("Unexpected rendering of IdRefListValue: %q", s)
	}

	// IdRefListValue accepts a mapper which aids in constructing human-readable strings.
	// The mapper does not need to have perfect coverage of the ID space, unresolved identifiers
	// are left as-is.
	v.Mapper = keyMap[int]{"life": 42, "bad-luck": 13}

	if s := v.String(); s != "life,7" {
		t.Fatalf("Unexpected rendering of IdRefListValue: %q", s)
	}

	if err := v.Set("bad-luck,life,4"); err != nil {
		t.Fatalf("Unexpected failure to set value: %v", err)
	}

	if !reflect.DeepEqual(l, []restapi.IdRef[int]{{ID: 13}, {ID: 42}, {ID: 4}}) {
		t.Fatalf("Unexpected IdRef slice after assignment: %v", l)
	}

	err := v.Set("life,potato")
	if err == nil {
		t.Fatalf("Unexpected success in setting value")
	}

	if err.Error() != "reference to unknown key potato" {
		t.Fatalf("Unexpected error in setting value: %v", err)
	}

	if !reflect.DeepEqual(l, []restapi.IdRef[int]{{ID: 13}, {ID: 42}, {ID: 4}}) {
		t.Fatalf("IdRef slice corrupted after failed assignment: %v", l)
	}
}
