// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package adapters

import (
	"strings"
)

// IdMapper translates between primary and alternate keys.
//
// The primary key is any type while alternate key is a kind of string. The
// mapper allows using the more-convenient alternate key and translating to the
// primary key when necessary.
//
// The environment type assists in using either keys. Many models do not need an
// environment mapper, as they exist globally. Some models are anchored at a
// specific instance of another model, and need the selector of the other model
// to be able to fetch the value identified by the primary or alternate keys.
type IdMapper[primaryT any, alternateT interface{ ~string }, mapEnvT any] interface {
	ToPrimary(alternateT, mapEnvT) (primaryT, error)
	ToAlternate(primaryT, mapEnvT) (alternateT, error)
}

// IdValue provides flag.Value for IDs with an optional IdMapper.
//
// In essence, it allows using ID values as command line arguments using the
// flag.FlagSet.Var function, with the added convenience of accepting
// human-friendly alternate keys and translating them on the fly.
type IdValue[primaryT interface{ String() string }, primaryPtrT interface {
	*primaryT
	Set(string) error
}, alternateT interface{ ~string }, mapEnvT any] struct {
	IdPtr  *primaryT
	Mapper IdMapper[primaryT, alternateT, mapEnvT]
	Env    mapEnvT
}

// String returns the value of the ID as a string.
//
// If the mapper is available the primary key is mapped to the alternate key.
// If the conversion fails or the mapper is unavailable a textual conversion
// of the primary key is used.
func (v IdValue[primaryT, primaryPtrT, alternateT, mapEnvT]) String() string {
	if v.IdPtr == nil {
		return ""
	}

	var s string

	if v.Mapper != nil {
		if alt, err := v.Mapper.ToAlternate(*v.IdPtr, v.Env); err == nil {
			s = string(alt)
		}
	}

	if s == "" {
		s = (*v.IdPtr).String()
	}

	return s
}

// Set assigns a new value to an identifier.
//
// If the value describes a number then that number is used as the new ID.
// If the mapper is available the value is assumed to be the alternate key
// and the a look-up is performed to find the corresponding primary key.
func (v IdValue[primaryT, primaryPtrT, alternateT, mapEnvT]) Set(s string) error {
	err := primaryPtrT(v.IdPtr).Set(s)
	if err == nil {
		return nil
	}

	if v.Mapper != nil {
		var id primaryT

		id, err = v.Mapper.ToPrimary(alternateT(s), v.Env)
		if err == nil {
			*v.IdPtr = id
		}
	}

	return err
}

// IdValue provides flag.Value for a list of IDs with an optional IdMapper.
//
// In essence, it allows using list of ID values as command line arguments using
// the flag.FlagSet.Var function, with the added convenience of accepting
// human-friendly alternate keys and translating them on the fly.
type IdListValue[primaryT interface{ String() string }, primaryPtrT interface {
	*primaryT
	Set(string) error
}, alternateT interface{ ~string }, mapEnvT any] struct {
	IdListPtr *[]primaryT
	Mapper    IdMapper[primaryT, alternateT, mapEnvT]
	Env       mapEnvT
}

// String returns a comma-separated list of identifiers.
//
// String behaves exactly the same like IdValue.String but produces
// a comma-separated list of identifiers.
func (v IdListValue[primaryT, primaryPtrT, alternateT, mapEnvT]) String() string {
	if v.IdListPtr == nil {
		return ""
	}

	var b strings.Builder

	for idx, id := range *v.IdListPtr {
		if idx > 0 {
			b.WriteByte(',')
		}

		var s string

		if v.Mapper != nil {
			if alt, err := v.Mapper.ToAlternate(id, v.Env); err == nil {
				s = string(alt)
			}
		}

		if s == "" {
			s = id.String()
		}

		b.WriteString(s)
	}

	return b.String()
}

// Set assigns a new value to a list of identifier.
//
// Set behaves exactly the same like IdValue.Set but works with a
// comma-separated list of identifiers.
func (v IdListValue[primaryT, primaryPtrT, alternateT, mapEnvT]) Set(s string) error {
	fields := strings.FieldsFunc(s, func(r rune) bool { return r == ',' })
	idList := make([]primaryT, 0, len(fields))

	for _, f := range fields {
		var id primaryT

		err := primaryPtrT(&id).Set(f)
		if err != nil && v.Mapper != nil {
			id, err = v.Mapper.ToPrimary(alternateT(f), v.Env)
		}

		if err != nil {
			return err
		}

		idList = append(idList, id)
	}

	*v.IdListPtr = idList

	return nil
}
