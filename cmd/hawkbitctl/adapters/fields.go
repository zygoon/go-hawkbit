// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package adapters

// Fields as they show up in user interface elements.
const (
	fldAddress                      = "Address"
	fldDeleted                      = "Deleted"
	fldDescription                  = "Description"
	fldID                           = "ID"
	fldInstalledAt                  = "Installed-At"
	fldIpAddress                    = "IP-Address"
	fldKey                          = "Key"
	fldLastControllerRequestAt      = "Last-Controller-Request-At"
	fldMandatorySoftwareModuleTypes = "Mandatory-Software-Module-Types"
	fldMaxAssignments               = "Max-Assignments"
	fldMd5Sum                       = "MD5-Sum"
	fldName                         = "Name"
	fldOptionalSoftwareModuleTypes  = "Optional-Software-Module-Types"
	fldRequestAttrs                 = "Request-Attributes"
	fldSecurityToken                = "Security-Token"
	fldSha1Sum                      = "SHA1-Sum"
	fldSha256Sum                    = "SHA256-Sum"
	fldSize                         = "Size"
	fldSoftwareModules              = "Software-Modules"
	fldType                         = "Type"
	fldUpdateStatus                 = "Update-Status"
	fldVendor                       = "Vendor"
	fldVersion                      = "Version"
)

// Fields as they show up in the REST API JSON representation.
const (
	jsAddress                 = "address"
	jsComplete                = "complete"
	jsControllerID            = "controllerID"
	jsCreatedAt               = "createdAt"
	jsCreatedBy               = "createdBy"
	jsDeleted                 = "deleted"
	jsDescription             = "description"
	jsID                      = "id"
	jsInstalledAt             = "installedAt"
	jsIpAddress               = "ipAddress"
	jsKey                     = "key"
	jsLastControllerRequestAt = "lastControllerRequestAt"
	jsLastModifiedAt          = "lastModifiedAt"
	jsLastModifiedBy          = "lastModifiedBy"
	jsName                    = "name"
	jsRequestAttributes       = "requestAttributes"
	jsRequiredMigrationStep   = "requiredMigrationStep"
	jsSecurityToken           = "securityToken"
	jsType                    = "type"
	jsUpdateStatus            = "updateStatus"
	jsVersion                 = "version"
)
