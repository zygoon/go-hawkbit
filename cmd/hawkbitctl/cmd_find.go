// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package main

import (
	"context"
	"flag"
	"fmt"
	"io"
	"sort"
	"strings"
	"text/tabwriter"

	"gitlab.com/zygoon/go-cmdr"

	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi"
	"gitlab.com/zygoon/go-hawkbit/pkg/restapi"
)

type findAdapter[findT any] interface {
	PluralName() string

	Find(context.Context, *mgmtapi.Client, restapi.FindOptions) (*restapi.Found[findT], error)
	ListHeader(io.Writer) error
	ListItem(io.Writer, *findT) error
	KnownFields() []string
}

// findItemsCmd provides a command for finding and listing items of a given type.
type findItemsCmd[findT any, adapterT findAdapter[findT]] struct {
	adapter adapterT
}

// NewFindItemsCmd returns a new find items command with the given adapter.
func NewFindItemsCmd[findT any, adapterT findAdapter[findT]](a adapterT) cmdr.Cmd {
	return &findItemsCmd[findT, adapterT]{adapter: a}
}

// OneLiner returns a one-line description of the command.
func (cmd findItemsCmd[findT, adapterT]) OneLiner() string {
	return fmt.Sprintf("find or list %s", cmd.adapter.PluralName())
}

// OrderingValue implements flag.Value for restapi.Ordering.
type OrderingValue struct {
	OrderingPtr *restapi.Ordering

	// If KnownFields is non-nil, then Set will only accept known fields.
	KnownFields StringSet
}

// String returns a comma-separated list of fields to sort by, with + and - indicating ordering.
func (v OrderingValue) String() string {
	if v.OrderingPtr == nil {
		return ""
	}

	var sb strings.Builder

	for i, fo := range *v.OrderingPtr {
		if i > 0 {
			sb.WriteByte(',')
		}

		if fo.Ascending {
			_ = sb.WriteByte('+')
		} else {
			_ = sb.WriteByte('-')
		}

		_, _ = sb.WriteString(fo.Field)
	}

	return sb.String()
}

// Set sets the ordering from a comma-separated list of fields, with + and - indicating ordering.
func (v OrderingValue) Set(s string) error {
	var ord restapi.Ordering

	for s != "" {
		// The field being parsed now.
		var field string

		// The parsed field order.
		var fo restapi.FieldOrder

		field, s, _ = strings.Cut(s, ",")

		switch {
		case strings.HasPrefix(field, "+"):
			fo.Ascending = true
			fo.Field = strings.TrimPrefix(field, "+")
		case strings.HasPrefix(field, "-"):
			fo.Ascending = false
			fo.Field = strings.TrimPrefix(field, "-")
		default:
			fo.Ascending = false
			fo.Field = field
		}

		if v.KnownFields != nil && !v.KnownFields.Contains(fo.Field) {
			return fmt.Errorf("unknown field %q, must be one of %s", fo.Field, v.KnownFields)
		}

		ord = append(ord, fo)
	}

	*v.OrderingPtr = ord

	return nil
}

func (cmd findItemsCmd[findT, adapterT]) Run(ctx context.Context, args []string) error {
	var findOpts restapi.FindOptions

	fs := flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)

	fs.StringVar(&findOpts.RawQ, "q", "", "Search query (FIQL)")
	fs.Var(OrderingValue{
		OrderingPtr: &findOpts.Sort,
		KnownFields: NewStringSet(cmd.adapter.KnownFields()),
	}, "o", "Sorting order ([+-]field,...)")
	fs.IntVar(&findOpts.Offset, "offset", 0, "Offset into the set of results")
	fs.IntVar(&findOpts.Limit, "limit", 25, "Number of results per page")

	_, stdout, stderr := cmdr.Stdio(ctx)
	fs.SetOutput(stderr)

	if err := fs.Parse(args); err != nil {
		return err
	}

	if fs.NArg() != 0 {
		return errUnexpectedArg
	}

	opts, err := optionsFromCtx(ctx)
	if err != nil {
		return err
	}

	cli, err := opts.Client()
	if err != nil {
		return err
	}

	w := tabwriter.NewWriter(stdout, 4, 4, 1, ' ', 0)

	if err := cmd.adapter.ListHeader(w); err != nil {
		return err
	}

	for {
		if err := cmd.adapter.ListHeader(w); err != nil {
			return err
		}

		found, err := cmd.adapter.Find(ctx, cli, findOpts)
		if err != nil {
			return err
		}

		for i := range found.Content {
			if err := cmd.adapter.ListItem(w, &found.Content[i]); err != nil {
				return err
			}
		}

		// Flush each page, so that we get speedy output regardless of the
		// number of results. This also avoids unbounded buffering.
		if err := w.Flush(); err != nil {
			return err
		}

		findOpts.Offset += found.Size

		if findOpts.Offset >= found.Total {
			break
		}
	}

	return nil
}

// StringSet is a set of strings.
type StringSet map[string]bool

// NewStringSet returns a set of strings out of a slice of strings.
func NewStringSet(ss []string) StringSet {
	m := make(map[string]bool, len(ss))

	for _, s := range ss {
		m[s] = true
	}

	return StringSet(m)
}

// Contains returns true if the given element is contained in the set.
func (ss StringSet) Contains(s string) bool {
	return ss[s]
}

// String returns a sorted, comma-separated list of strings contained in the set.
func (ss StringSet) String() string {
	x := make([]string, 0, len(ss))

	for s := range ss {
		x = append(x, s)
	}

	sort.Strings(x)

	return strings.Join(x, ",")
}
