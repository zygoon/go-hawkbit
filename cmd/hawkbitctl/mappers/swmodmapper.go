// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package mappers

import (
	"context"
	"errors"

	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/swmods"
)

var errSoftwareModuleNotFound = errors.New("software module not found")

func NewSoftwareModuleNameVersionMapper(ctx context.Context, loader ClientLoader) *NameVersionMapper[swmods.ID, swmods.Data, swmods.FindData, swmods.Ref, swmods.Model] {
	modelFn := func(cli *mgmtapi.Client) swmods.Model { return cli.SoftwareModuleModel() }

	return NewNameVersionMapper[swmods.ID, swmods.Data, swmods.FindData, swmods.Ref](
		ctx, loader, modelFn, errSoftwareModuleNotFound)
}
