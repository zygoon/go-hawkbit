// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package mappers

import (
	"context"
	"errors"

	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/dses"
)

var errDistributionSetNotFound = errors.New("distribution set not found")

func NewDistributionSetNameVersionMapper(ctx context.Context, loader ClientLoader) *NameVersionMapper[dses.ID, dses.Data, dses.FindData, dses.Ref, dses.Model] {
	modelFn := func(cli *mgmtapi.Client) dses.Model { return cli.DistributionSetModel() }

	return NewNameVersionMapper[dses.ID, dses.Data, dses.FindData, dses.Ref](
		ctx, loader, modelFn, errDistributionSetNotFound)
}
