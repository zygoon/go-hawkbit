// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package mappers

import (
	"context"
	"errors"
	"fmt"

	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/swmodtypes"
	"gitlab.com/zygoon/go-hawkbit/pkg/restapi"
)

var errSoftwareModuleTypeNotFound = errors.New("software module type not found")

type ClientLoader func(context.Context) (*mgmtapi.Client, error)

type SoftwareModuleTypeKeyMapper struct {
	ctx    context.Context
	loader ClientLoader
	// TODO: cache requests?
}

func NewSoftwareModuleTypeKeyMapper(ctx context.Context, loader ClientLoader) *SoftwareModuleTypeKeyMapper {
	return &SoftwareModuleTypeKeyMapper{ctx: ctx, loader: loader}
}

func (m *SoftwareModuleTypeKeyMapper) ToPrimary(key string, env struct{}) (swmodtypes.ID, error) {
	cli, err := m.loader(m.ctx)
	if err != nil {
		return 0, err
	}

	opts := restapi.FindOptions{
		Limit: 1,
		// TODO: provide reference to the query language.
		// FIXME: there's no escaping for the key here.
		RawQ: fmt.Sprintf("key==%s", key),
	}

	found, err := cli.SoftwareModuleTypeModel().Find(m.ctx, cli.Client, opts)
	if err != nil {
		return 0, err
	}

	if found.Total != 1 {
		return 0, errSoftwareModuleTypeNotFound
	}

	return found.Content[0].ID, nil
}

func (m *SoftwareModuleTypeKeyMapper) ToAlternate(id swmodtypes.ID, env struct{}) (string, error) {
	cli, err := m.loader(m.ctx)
	if err != nil {
		return "", err
	}

	smt, err := cli.SoftwareModuleTypeModel().Ref(id).Get(m.ctx, cli.Client)
	if err != nil {
		return "", err
	}

	return smt.Key, nil
}
