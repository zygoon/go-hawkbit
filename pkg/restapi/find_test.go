// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package restapi_test

import (
	"context"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"

	"gitlab.com/zygoon/go-hawkbit/pkg/restapi"
)

type findResp struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type namer struct{}

func (namer) ResourceName() string {
	return "resource"
}

func (namer) ResourcePluralName() string {
	return "resources"
}

func TestNamer(t *testing.T) {
	n := namer{}

	if rn := n.ResourceName(); rn != "resource" {
		t.Fatalf("Unexpected resource name: %q", rn)
	}

	if rpn := n.ResourcePluralName(); rpn != "resources" {
		t.Fatalf("Unexpected plural resource name: %q", rpn)
	}
}

func TestFind(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		if s := req.URL.RawQuery; s != "limit=20&offset=10&q=name%3D%3Dfoo" {
			t.Fatalf("Unexpected raw query: %q", s)
		}
		rw.Header().Add("Content-Type", "application/hal+json;charset=utf-8")
		rw.WriteHeader(200)
		_, err := rw.Write([]byte(`{
			"total": 1,
			"size": 1,
			"content": [{
					"id": 25,
					"name": "foo"
			}]
		}`))

		if err != nil {
			t.Fatalf("Unexpected failure: %v", err)
		}
	}))

	cli := restapi.NewClient("user", "pass")

	found, err := restapi.FindResources[findResp](context.Background(), cli, srv.URL, restapi.FindOptions{Offset: 10, Limit: 20, RawQ: "name==foo"})
	if err != nil {
		t.Fatalf("Unexpected failure: %v", err)
	}

	if !reflect.DeepEqual(found, &restapi.Found[findResp]{
		Total: 1,
		Size:  1,
		Content: []findResp{{
			ID:   25,
			Name: "foo",
		}},
	}) {
		t.Fatalf("Unexpected resources found: %#v", found)
	}
}
