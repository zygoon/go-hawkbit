// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package restapi

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"gitlab.com/zygoon/go-hawkbit/pkg/contenttype"
	"gitlab.com/zygoon/go-hawkbit/pkg/headers"
)

// Identifier uniquely identifiers a resource in a group.
type Identifier interface {
	~int | ~string
}

func do(ctx context.Context, cli *Client, meth, href string, body io.Reader, maybeContentType ...string) (*http.Response, error) {
	req, err := http.NewRequestWithContext(ctx, meth, href, body)
	if err != nil {
		return nil, err
	}

	if body != nil {
		if len(maybeContentType) > 0 {
			req.Header.Add(headers.ContentType, maybeContentType[0])
		} else {
			req.Header.Add(headers.ContentType, contenttype.AppHalJsonUtf8)
		}
	}

	req.Header.Add(headers.Accept, contenttype.AppHalJson)
	cli.authorizer.AuthorizeRequest(req)

	resp, err := cli.Do(req)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

func decode[T any](resp *http.Response) (*T, error) {
	ct, _, err := contenttype.Parse(resp.Header.Get(headers.ContentType))
	if err != nil {
		return nil, err
	}

	// When the response is empty, Content-Type is not getting set.
	if resp.ContentLength == 0 && ct == "" {
		return new(T), nil
	}

	if ct != contenttype.AppHalJson {
		return nil, UnexpectedContentType(ct)
	}

	var data T
	if err := json.NewDecoder(resp.Body).Decode(&data); err != nil {
		return nil, err
	}

	return &data, nil
}

func failure(resp *http.Response) error {
	ct, _, err := contenttype.Parse(resp.Header.Get(headers.ContentType))
	if err != nil {
		return err
	}

	if ct == contenttype.AppHalJson {
		return decodeError(resp)
	}

	return UnexpectedStatusCode(resp.StatusCode)
}

func decodeError(resp *http.Response) error {
	var errInfo Error

	// Buffer error body in case we need to decode it twice.
	data, err := io.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	if err := json.Unmarshal(data, &errInfo); err != nil {
		return err
	}

	if errInfo.Class == "" || errInfo.Code == "" {
		// We probably didn't get a HawkBit error response.
		// Try again with the other error type.
		var springErr maybeSpringError

		if err := json.Unmarshal(data, &springErr); err != nil {
			return err
		}

		if springErr.TimeStamp != "" {
			return &springErr
		}
	}

	return &errInfo
}

// UnexpectedContentType records unexpected Content-Type header.
type UnexpectedContentType string

// Error implements the error interface.
func (e UnexpectedContentType) Error() string {
	return fmt.Sprintf("unexpected content type: %s", string(e))
}

// UnexpectedStatusCode records unexpected HTTP status code.
type UnexpectedStatusCode int

// Error implements the error interface.
func (e UnexpectedStatusCode) Error() string {
	return fmt.Sprintf("unexpected response status: %s (%d)", http.StatusText(int(e)), int(e))
}
