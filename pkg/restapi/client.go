// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package restapi

import (
	"net/http"
)

// Client is a http.Client with HawkBit credentials.
type Client struct {
	http.Client

	authorizer requestAuthorizer
}

// requestAuthorizer is an interface wrapping AuthorizeRequest.
//
// Authorizer is used by restapi.Client to carry information on how to set
// request credentials.
type requestAuthorizer interface {
	AuthorizeRequest(*http.Request)
}

// NewClientWithAuthorizer creates a client with the given request authorizer.
//
// Authorizer is one of: Account, TenantAccount, GatewayToken or TargetToken.
func NewClientWithAuthorizer(a requestAuthorizer) *Client {
	return &Client{authorizer: a}
}

// NewClient creates a new client with account credentials.
func NewClient(username, password string) *Client {
	return NewClientWithAuthorizer(&Account{Username: username, Password: password})
}

// NewTenantClient creates a new client with tenant-specific account credentials.
func NewTenantClient(tenant, username, password string) *Client {
	return NewClientWithAuthorizer(&TenantAccount{Tenant: tenant, Username: username, Password: password})
}
