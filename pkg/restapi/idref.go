// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package restapi

// IdRef is a reference to a foreign key.
//
// HawkBit uses an indirection, where foreign key references are represented
// as JSON objects with a single "id" attribute.
type IdRef[T interface{ ~int }] struct {
	ID T `json:"id"`
}
