// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package restapi

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
)

// PostResource POSTs a resource to a given URL.
//
// The type parameters dataT and dataPtrT describe the resource that is being
// sent. The server is expected to respond with http.StatusOk and not send any
// response data.
func PostResource[dataT any, dataPtrT interface {
	*dataT
	ContentType() string
}](ctx context.Context, cli *Client, href string, data *dataT) error {
	blob, err := json.MarshalIndent(data, "", "  ")
	if err != nil {
		return err
	}

	ct := dataPtrT(data).ContentType()

	resp, err := do(ctx, cli, http.MethodPost, href, bytes.NewReader(blob), ct)
	if err != nil {
		return err
	}

	defer func() {
		_ = resp.Body.Close()
	}()

	if resp.StatusCode != http.StatusOK {
		return failure(resp)
	}

	return nil
}

// CreateResources creates one or more resources with a single request.
//
// The type parameters inT and outT describe the resource that is being
// created and the response from the server. Typically the request is a subset
// of the response. The response may contain assigned primary keys and
// meta-data.
func CreateResources[inT, outT any](ctx context.Context, cli *Client, href string, items []inT) ([]outT, error) {
	data, err := json.Marshal(items)
	if err != nil {
		return nil, err
	}

	resp, err := do(ctx, cli, http.MethodPost, href, bytes.NewReader(data))
	if err != nil {
		return nil, err
	}

	defer func() {
		_ = resp.Body.Close()
	}()

	if resp.StatusCode != http.StatusCreated {
		return nil, failure(resp)
	}

	v, err := decode[[]outT](resp)
	if err != nil {
		return nil, err
	}

	return *v, nil
}

// ReadResource retrieves the resource with the given href.
//
// Type parameter T describes the response from the server.
func ReadResource[T any](ctx context.Context, cli *Client, href string) (*T, error) {
	resp, err := do(ctx, cli, http.MethodGet, href, nil)
	if err != nil {
		return nil, err
	}

	defer func() {
		_ = resp.Body.Close()
	}()

	if resp.StatusCode != http.StatusOK {
		return nil, failure(resp)
	}

	return decode[T](resp)
}

// ReadResourceSlice retrieves a slice of resources from the given href.
//
// Type parameter T describes the individual element.
func ReadResourceSlice[T any](ctx context.Context, cli *Client, href string) ([]T, error) {
	resp, err := do(ctx, cli, http.MethodGet, href, nil)
	if err != nil {
		return nil, err
	}

	defer func() {
		_ = resp.Body.Close()
	}()

	if resp.StatusCode != http.StatusOK {
		return nil, failure(resp)
	}

	v, err := decode[[]T](resp)
	if err != nil {
		return nil, err
	}

	return *v, err
}

// UpdateResource updates the resource with the given href.
//
// Type parameter inT describes the data sent in the PUT request. Type
// parameter outT describes the response from the server. Typically this is a
// snapshot of the modified resource.
func UpdateResource[inT any, outT any, inPtrT interface {
	*inT
	ContentType() string
}](ctx context.Context, cli *Client, href string, up inPtrT) (*outT, error) {
	data, err := json.Marshal(up)
	if err != nil {
		return nil, err
	}

	ct := up.ContentType()

	resp, err := do(ctx, cli, http.MethodPut, href, bytes.NewReader(data), ct)
	if err != nil {
		return nil, err
	}

	defer func() {
		_ = resp.Body.Close()
	}()

	if resp.StatusCode != http.StatusOK {
		return nil, failure(resp)
	}

	return decode[outT](resp)
}

// DeleteResource deletes the resource with the given href.
func DeleteResource(ctx context.Context, cli *Client, href string) error {
	resp, err := do(ctx, cli, http.MethodDelete, href, nil)
	if err != nil {
		return err
	}

	defer func() {
		_ = resp.Body.Close()
	}()

	if resp.StatusCode != http.StatusOK {
		return failure(resp)
	}

	return nil
}

// AddRelation adds a relation between two existing resources.
//
// href is the URL of the resource on the near side of the relation. ID is the
// of the foreign key, or the identifier on the far side of the relation.
func AddRelation[idT Identifier](ctx context.Context, cli *Client, href string, id idT) error {
	// TODO: this marshals exactly the same way and IdRef would, we could avoid
	// having the hand-rolled map here.
	data, err := json.Marshal(map[string]any{"id": id})
	if err != nil {
		return err
	}

	resp, err := do(ctx, cli, http.MethodPost, href, bytes.NewReader(data))
	if err != nil {
		return err
	}

	defer func() {
		_ = resp.Body.Close()
	}()

	if resp.StatusCode != http.StatusCreated {
		return failure(resp)
	}

	return nil
}
