// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package swmods

import (
	"context"

	"gitlab.com/zygoon/go-hawkbit/pkg/contenttype"
	"gitlab.com/zygoon/go-hawkbit/pkg/errprefix"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/artifacts"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/metadata"
	"gitlab.com/zygoon/go-hawkbit/pkg/restapi"
)

// Ref is an URL to a specific software module.
type Ref string

// Get retrieves software module data.
//
// This API is documented at https://www.eclipse.org/hawkbit/rest-api/softwaremodules-api-guide/#_get_rest_v1_softwaremodules_softwaremoduleid
func (r Ref) Get(ctx context.Context, cli *restapi.Client) (*Data, error) {
	v, err := restapi.ReadResource[Data](ctx, cli, string(r))
	if err != nil {
		return nil, errprefix.NewError[restapi.ReadErrorPrefix[Namer]](err)
	}

	return v, nil
}

// Update modifies a subset of the software module data.
//
// This API is documented at https://www.eclipse.org/hawkbit/rest-api/softwaremodules-api-guide/#_put_rest_v1_softwaremodules_softwaremoduleid
func (r Ref) Update(ctx context.Context, cli *restapi.Client, up *UpdateData) (*Data, error) {
	v, err := restapi.UpdateResource[UpdateData, Data](ctx, cli, string(r), up)
	if err != nil {
		return nil, errprefix.NewError[restapi.UpdateErrorPrefix[Namer]](err)
	}

	return v, nil
}

// Delete deletes the software module.
//
// This API is documented at https://www.eclipse.org/hawkbit/rest-api/softwaremodules-api-guide/#_delete_rest_v1_softwaremodules_softwaremoduleid
func (r Ref) Delete(ctx context.Context, cli *restapi.Client) error {
	if err := restapi.DeleteResource(ctx, cli, string(r)); err != nil {
		return errprefix.NewError[restapi.DeleteErrorPrefix[Namer]](err)
	}

	return nil
}

// ArtifactModel returns the artifacts model for the given software module.
func (r Ref) ArtifactModel() artifacts.Model {
	return artifacts.Model(r + "/artifacts")
}

// MetaDataEntry describes a single meta-data entry.
//
// This API is documented at https://www.eclipse.org/hawkbit/rest-api/softwaremodules-api-guide/#_post_rest_v1_softwaremodules_softwaremoduleid_metadata
type MetaDataEntry struct {
	Key           string `json:"key"`
	Value         string `json:"value"`
	TargetVisible bool   `json:"targetVisible,omitempty"`
}

func (*MetaDataEntry) ContentType() string {
	return contenttype.AppHalJson
}

// MetaDataModel returns the model for meta-data of a specific target.
func (r Ref) MetaDataModel() metadata.Model[MetaDataEntry, *MetaDataEntry] {
	return metadata.Model[MetaDataEntry, *MetaDataEntry](r + metadata.PathSuffix)
}
