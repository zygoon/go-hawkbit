// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package swmodtypes

import (
	"context"
	"strings"

	"gitlab.com/zygoon/go-hawkbit/pkg/restapi"
)

// ManyRelation is an URL to many-to-many relation with software module types.
//
// Individual relations may have different URLs and may separately contain a
// given software module type.
type ManyRelation string

// Member returns the ManyMember with the given software module type ID.
func (m ManyRelation) Member(id ID) ManyMember {
	var b strings.Builder

	idStr := id.String()
	b.Grow(len(m) + 1 + len(idStr))

	b.WriteString(string(m))
	b.WriteByte('/')
	b.WriteString(idStr)

	return ManyMember(b.String())
}

// Add adds an existing software module type, with the given ID, to the relation.
func (m ManyRelation) Add(ctx context.Context, cli *restapi.Client, id ID) error {
	return restapi.AddRelation(ctx, cli, string(m), id)
}

// All returns all related software module types.
func (m ManyRelation) All(ctx context.Context, cli *restapi.Client) ([]Data, error) {
	return restapi.ReadResourceSlice[Data](ctx, cli, string(m))
}

// ManyMember is an URL of a software module type inside a particular relation.
type ManyMember string

// Get retrieves software module type data.
func (m ManyMember) Get(ctx context.Context, cli *restapi.Client) (*Data, error) {
	return restapi.ReadResource[Data](ctx, cli, string(m))
}

// Remove removes an existing software module type from the relation.
func (m ManyMember) Remove(ctx context.Context, cli *restapi.Client) error {
	return restapi.DeleteResource(ctx, cli, string(m))
}
