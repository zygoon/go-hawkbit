// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package actions

import (
	"context"
	"strings"

	"gitlab.com/zygoon/go-hawkbit/pkg/errprefix"
	"gitlab.com/zygoon/go-hawkbit/pkg/restapi"
)

// Model is an URL to a collection of actions.
type Model string

// Ref returns the link to an action with a given ID.
func (m Model) Ref(id ID) Ref {
	var b strings.Builder

	idStr := id.String()

	b.Grow(len(m) + 1 + len(idStr))
	b.WriteString(string(m))
	b.WriteByte('/')
	b.WriteString(idStr)

	return Ref(b.String())
}

// Find searches or enumerates actions.
//
// FindOptions define ordering, search and the size and offset of the returned data set.
// Multiple calls to Find are usually required to traverse all results.
//
// This API is documented at https://eclipse.dev/hawkbit/rest-api/mgmt.html#tag/Actions/operation/getActions
func (m Model) Find(ctx context.Context, cli *restapi.Client, opts restapi.FindOptions) (*restapi.Found[FindData], error) {
	v, err := restapi.FindResources[FindData](ctx, cli, string(m), opts)
	if err != nil {
		return nil, errprefix.NewError[restapi.FindErrorPrefix[Namer]](err)
	}

	return v, nil
}
