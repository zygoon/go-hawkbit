// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package actionstatuses

import (
	"context"

	"gitlab.com/zygoon/go-hawkbit/pkg/errprefix"
	"gitlab.com/zygoon/go-hawkbit/pkg/restapi"
)

// Model is an URL to a collection of action status entries.
type Model string

// Find searches or enumerates action status entries.
//
// FindOptions define ordering, search and the size and offset of the returned data set.
// Multiple calls to Find are usually required to traverse all results.
//
// This API is documented at https://eclipse.dev/hawkbit/rest-api/mgmt.html#tag/Targets/operation/getActionStatusList
func (m Model) Find(ctx context.Context, cli *restapi.Client, opts restapi.FindOptions) (*restapi.Found[FindData], error) {
	v, err := restapi.FindResources[FindData](ctx, cli, string(m), opts)
	if err != nil {
		return nil, errprefix.NewError[restapi.FindErrorPrefix[Namer]](err)
	}

	return v, nil
}
