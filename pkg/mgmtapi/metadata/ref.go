// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package metadata

import (
	"context"

	"gitlab.com/zygoon/go-hawkbit/pkg/errprefix"
	"gitlab.com/zygoon/go-hawkbit/pkg/restapi"
)

// Ref is an URL to a specific meta-data entry.
//
// The type parameter EntryT is the concrete type of the meta-data entry.
type Ref[EntryT any, EntryPtrT interface {
	*EntryT
	ContentType() string
}] string

// Get retrieves meta-data entry.
func (r Ref[EntryT, EntryPtrT]) Get(ctx context.Context, cli *restapi.Client) (*EntryT, error) {
	v, err := restapi.ReadResource[EntryT](ctx, cli, string(r))
	if err != nil {
		return nil, errprefix.NewError[restapi.ReadErrorPrefix[Namer]](err)
	}

	return v, nil
}

// Update modifies a meta-data entry.
func (r Ref[EntryT, EntryPtrT]) Update(ctx context.Context, cli *restapi.Client, up *EntryT) (*EntryT, error) {
	v, err := restapi.UpdateResource[EntryT, EntryT, EntryPtrT](ctx, cli, string(r), up)
	if err != nil {
		return nil, errprefix.NewError[restapi.UpdateErrorPrefix[Namer]](err)
	}

	return v, nil
}

// Delete deletes the meta-data entry.
func (r Ref[EntryT, EntryPtrT]) Delete(ctx context.Context, cli *restapi.Client) error {
	if err := restapi.DeleteResource(ctx, cli, string(r)); err != nil {
		return errprefix.NewError[restapi.DeleteErrorPrefix[Namer]](err)
	}

	return nil
}
