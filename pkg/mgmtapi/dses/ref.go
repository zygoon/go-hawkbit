// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package dses

import (
	"context"

	"gitlab.com/zygoon/go-hawkbit/pkg/contenttype"
	"gitlab.com/zygoon/go-hawkbit/pkg/errprefix"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/metadata"
	"gitlab.com/zygoon/go-hawkbit/pkg/restapi"
)

// Ref is an URL to a specific distribution set.
type Ref string

// Get retrieves distribution set data.
//
// This API is documented at https://www.eclipse.org/hawkbit/rest-api/distributionsets-api-guide/#_get_rest_v1_distributionsets_distributionsetid
func (r Ref) Get(ctx context.Context, cli *restapi.Client) (*Data, error) {
	v, err := restapi.ReadResource[Data](ctx, cli, string(r))
	if err != nil {
		return nil, errprefix.NewError[restapi.ReadErrorPrefix[Namer]](err)
	}

	return v, nil
}

// Update modifies a subset of the distribution set data.
//
// This API is documented at https://www.eclipse.org/hawkbit/rest-api/distributionsets-api-guide/#_put_rest_v1_distributionsets_distributionsetid
func (r Ref) Update(ctx context.Context, cli *restapi.Client, up *UpdateData) (*Data, error) {
	v, err := restapi.UpdateResource[UpdateData, Data](ctx, cli, string(r), up)
	if err != nil {
		return nil, errprefix.NewError[restapi.UpdateErrorPrefix[Namer]](err)
	}

	return v, nil
}

// Delete deletes the distribution set.
//
// This API is documented at https://www.eclipse.org/hawkbit/rest-api/distributionsets-api-guide/#_delete_rest_v1_distributionsets_distributionsetid
func (r Ref) Delete(ctx context.Context, cli *restapi.Client) error {
	if err := restapi.DeleteResource(ctx, cli, string(r)); err != nil {
		return errprefix.NewError[restapi.DeleteErrorPrefix[Namer]](err)
	}

	return nil
}

// MetaDataEntry describes a single meta-data entry.
//
// This API is documented at https://www.eclipse.org/hawkbit/rest-api/distributionsets-api-guide/#_post_rest_v1_distributionsets_distributionsetid_metadata
type MetaDataEntry struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

func (*MetaDataEntry) ContentType() string {
	return contenttype.AppHalJson
}

// MetaDataModel returns the model for meta-data of a specific target.
func (r Ref) MetaDataModel() metadata.Model[MetaDataEntry, *MetaDataEntry] {
	return metadata.Model[MetaDataEntry, *MetaDataEntry](r + metadata.PathSuffix)
}
