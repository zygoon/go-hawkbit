// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package dses contains type definitions for working with Distribution Sets.
//
// A distribution set is a set of software modules pinned at specific revisions,
// applicable to be installed onto targets.
//
// The implementation is based on the official documentation at
// https://www.eclipse.org/hawkbit/apis/mgmt/distributionsets/
package dses

import (
	"strconv"

	"gitlab.com/zygoon/go-hawkbit/pkg/contenttype"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/dstypes"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/swmods"
	"gitlab.com/zygoon/go-hawkbit/pkg/restapi"
)

// Namer implements ResourceNamer for distribution sets.
type Namer struct{}

// ResourceName returns the singular name of the type used by error prefix types.
func (Namer) ResourceName() string {
	return "distribution set"
}

// ResourcePluralName returns the plural type name used by error prefix types.
func (Namer) ResourcePluralName() string {
	return "distribution sets"
}

// CreateData describes a distribution set to create.
type CreateData struct {
	Name        string `json:"name"`
	Description string `json:"description,omitempty"`

	RequiredMigrationStep bool                       `json:"requiredMigrationStep"`
	Type                  string                     `json:"type"` // Refers to a key of a distribution set type.
	Version               string                     `json:"version"`
	Modules               []restapi.IdRef[swmods.ID] `json:"modules"` // Refers to software modules.
}

// ID is the primary key for distribution sets.
type ID int

// String returns the ID as a string.
func (id ID) String() string {
	return strconv.Itoa(int(id))
}

// Set sets the ID to the given string value.
//
// The value must be a decimal representation of a number.
func (id *ID) Set(s string) error {
	n, err := strconv.Atoi(s)
	if err != nil {
		return err
	}

	*id = ID(n)

	return nil
}

// Data describes a retrieved or created distribution set.
type Data struct {
	ID ID `json:"id"`

	Name        string `json:"name"`
	Description string `json:"description,omitempty"`

	Complete              bool              `json:"complete"`
	RequiredMigrationStep bool              `json:"requiredMigrationStep"`
	Type                  string            `json:"type"` // Refers to a key of a distribution set type.
	Version               string            `json:"version"`
	Modules               []swmods.FindData `json:"modules"` // Actual modules, a bit weird.

	CreatedBy      string `json:"createdBy,omitempty"`
	CreatedAt      int64  `json:"createdAt,omitempty"`
	LastModifiedBy string `json:"lastModifiedBy,omitempty"`
	LastModifiedAt int64  `json:"lastModifiedAt,omitempty"`
	Deleted        bool   `json:"deleted,omitempty"`

	Links struct {
		Self restapi.Link `json:"self"`
		Type restapi.Link `json:"type"`
		// TODO: modules link
		// TODO: metadata link
	} `json:"_links,omitempty"`
}

// Self returns the ref to the distribution set.
func (d *Data) SelfRef() Ref {
	return Ref(d.Links.Self.Href)
}

// TypeRef returns the ref to the distribution set type.
func (d *Data) TypeRef() dstypes.Ref {
	return dstypes.Ref(d.Links.Type.Href)
}

// AlternateID returns the unique "name:version" pair.
//
// This method exists as a work-around for https://github.com/golang/go/issues/48522
// and should be removed once Go can access structure fields from generic functions.
func (d Data) AlternateID() string {
	return d.Name + ":" + d.Version
}

// FindData describes a found distribution set.
//
// FindData is usually a subset of Data.
type FindData struct {
	ID ID `json:"id"`

	Name        string `json:"name"`
	Description string `json:"description,omitempty"`

	Complete              bool   `json:"complete"`
	RequiredMigrationStep bool   `json:"requiredMigrationStep"`
	Type                  string `json:"type"` // Refers to a key of a distribution set type.
	Version               string `json:"version"`

	CreatedBy      string `json:"createdBy,omitempty"`
	CreatedAt      int64  `json:"createdAt,omitempty"`
	LastModifiedBy string `json:"lastModifiedBy,omitempty"`
	LastModifiedAt int64  `json:"lastModifiedAt,omitempty"`
	Deleted        bool   `json:"deleted,omitempty"`

	Links struct {
		Self restapi.Link `json:"self"`
	} `json:"_links,omitempty"`
}

// PrimaryID returns the ID.
//
// This method exists as a work-around for https://github.com/golang/go/issues/48522
// and should be removed once Go can access structure fields from generic functions.
func (d FindData) PrimaryID() ID {
	return d.ID
}

// UpdateData describes updates to a distribution set type.
//
// Nil fields are not updated and can be left out.
type UpdateData struct {
	Name        *string `json:"name,omitempty"`
	Description *string `json:"description,omitempty"`

	Version               *string `json:"version,omitempty"`
	RequiredMigrationStep *bool   `json:"requiredMigrationStep,omitempty"`
}

func (*UpdateData) ContentType() string {
	return contenttype.AppHalJson
}
