// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package artifacts contains type definitions for working with Artifacts.
//
// Artifacts are associated with specific software modules, and don't exist as a
// standalone top-level collection. Their Model API differs slightly from other
// models, in that there is no support for search, paging or ordering.
//
// The implementation is based on the official documentation at
// https://eclipse.dev/hawkbit/rest-api/mgmt.html#tag/Software-Modules/operation/getArtifact
package artifacts

import (
	"strconv"

	"gitlab.com/zygoon/go-hawkbit/pkg/restapi"
)

// Namer implements ResourceNamer for artifacts.
type Namer struct{}

// ResourceName returns the singular name of the type used by error prefix types.
func (Namer) ResourceName() string {
	return "artifact"
}

// ResourcePluralName returns the plural type name used by error prefix types.
func (Namer) ResourcePluralName() string {
	return "artifacts"
}

// ID is the primary key for artifacts.
type ID int

// String returns the ID as a string.
func (id ID) String() string {
	return strconv.Itoa(int(id))
}

// Set sets the ID to the given string value.
//
// The value must be a decimal representation of a number.
func (id *ID) Set(s string) error {
	n, err := strconv.Atoi(s)
	if err != nil {
		return err
	}

	*id = ID(n)

	return nil
}

// Data describes a retrieved or created artifact.
type Data struct {
	ID               ID     `json:"id"`
	Size             int    `json:"size"`
	ProvidedFileName string `json:"providedFilename"`

	CreatedBy      string `json:"createdBy,omitempty"`
	CreatedAt      int64  `json:"createdAt,omitempty"`
	LastModifiedBy string `json:"lastModifiedBy,omitempty"`
	LastModifiedAt int64  `json:"lastModifiedAt,omitempty"`

	Hashes struct {
		Md5Sum    string `json:"md5"`
		Sha1Sum   string `json:"sha1"`
		Sha256Sum string `json:"sha256"`
	} `json:"hashes"`

	Links struct {
		Self     restapi.Link `json:"self"`
		Download restapi.Link `json:"download"`
	} `json:"_links,omitempty"`
}
