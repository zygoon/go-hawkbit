// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package artifacts

import (
	"context"

	"gitlab.com/zygoon/go-hawkbit/pkg/errprefix"
	"gitlab.com/zygoon/go-hawkbit/pkg/restapi"
)

// Ref is an URL to a specific artifact.
type Ref string

// Get retrieves artifact data.
//
// This API is documented at
// https://eclipse.dev/hawkbit/rest-api/mgmt.html#tag/Software-Modules/operation/getArtifact
func (r Ref) Get(ctx context.Context, cli *restapi.Client) (*Data, error) {
	v, err := restapi.ReadResource[Data](ctx, cli, string(r))
	if err != nil {
		return nil, errprefix.NewError[restapi.ReadErrorPrefix[Namer]](err)
	}

	return v, nil
}

// Delete deletes the artifact.
//
// This API is documented at
// https://eclipse.dev/hawkbit/rest-api/mgmt.html#tag/Software-Modules/operation/deleteArtifact
func (r Ref) Delete(ctx context.Context, cli *restapi.Client) error {
	if err := restapi.DeleteResource(ctx, cli, string(r)); err != nil {
		return errprefix.NewError[restapi.DeleteErrorPrefix[Namer]](err)
	}

	return nil
}
