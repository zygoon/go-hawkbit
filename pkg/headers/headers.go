// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package headers contains constants for some HTTP headers.
package headers

const (
	// ContentType is the "Content-Type" header.
	ContentType = "Content-Type"
	// Accept is the "Accept" header.
	Accept = "Accept"
	// Authorization is the "Authorization" header.
	Authorization = "Authorization"
)
