<!--
SPDX-License-Identifier: Apache-2.0
SPDX-FileCopyrightText: Huawei Inc.
-->

# The hawkbitctl snap

[![hawkbitctl](https://snapcraft.io/hawkbitctl/badge.svg)](https://snapcraft.io/hawkbitctl)

This directory contains the snap packaging for the `hawkbitctl` tool. Any
changes made to the official upstream git repository at
https://gitlab.com/zygoon/go-hawkbit, are automatically built by Launchpad and
published to the snap store. The packaging recipe is at
https://launchpad.net/~zyga/hawkbitctl/+snap/hawkbitctl

The built snap is published to the `latest/edge` channel. Tagged releases
should be auto-built or remote-built and published to specific channels
manually.

## Development and local testing

You can build the snap package locally with `snapcraft`. See snapcraft
documentation for basic information about how to do this.

## State

The snap has no state or configuration at this time.
